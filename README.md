# 20240505-BhushanKasturiwale-NYCSchools



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bk9272518/20240505-bhushankasturiwale-nycschools.git
git branch -M main
git push -uf origin main
```


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/bk9272518/20240505-bhushankasturiwale-nycschools/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Project Requirement
Native Android project which displays the list of school in New York City.

It shows details like
- School Name
- Description
- Contact Details including email, phone, website
- Address of the School
- Academic Opportunities
- Extracurricular Activities
- Sports
- School Score in fields like Math, Reading and Writing


## Project Architecture
Project is developed using latest Android Technology and following are some salient features
- Gradle for dependencies
- libs.version.toml is the file use for maintaining versions of the dependency used
- Near to no third party libraries used
- Project UI is built with Jetpack Compose, by following Single Activity architecture
- Compose is used for screen navigation
- Separation of concern is followed with different components for the screens and UI elements
- MVVM Architecture is followed with View Models handling most of the data logic part
- Hilt used for dependency injection
- Unit Test Cases added for the View Model classes

## Project Screen Flow
- Application on launch shows a splash screen for a moment
- School list screen is shown after that which comes with progress bar at start
- List of schools with school name is displayed
- Search functionality is provided for the users to search the school
- Selecting any school shows school details page and its score values
- Application supports landscape mode.
- Light and Dark Theme supported
- Accessibility considered for certain level but not thorough


## Future Work
- Add more fields related to school details
- Adding alpha numeric scroller to the school list screen
- Setting a school as favorite and a different for quick access to favorite schools
- User actions
    - Initiating a call from the app,
    - Launching an email from the app with pre-populated email address
    - Launching the school website
    - Launching google map showing the school in the map
- Offline support
- Snack Bar showing that device has lost connection