package com.jpc.nyc.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.jpc.nyc.R

@Composable
fun LabelTextWithIcon(
    text: String?,
    icon: ImageVector?,
    contentDescription: String? = "",
    isWebLink: Boolean = false,
    onClick: (String) -> Unit = {}
) {
    text?.let{ label ->
        Row(
            modifier = Modifier.clickable { onClick(label) }
        ) {
            icon?.let {
                Icon(
                    imageVector = it,
                    modifier = Modifier
                        .size(24.dp)
                        .padding(end = 8.dp),
                    contentDescription = contentDescription
                )
            }
            Text(
                text = label,
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.poppins_light)),
                color = if(isWebLink) Color.Blue else MaterialTheme.colorScheme.secondary,
                textDecoration = if(isWebLink) TextDecoration.Underline else null,
            )
        }

        Spacer(modifier = Modifier.height(8.dp))
    }
}

@Composable
fun BodyTitleText(text: String?) {
    text?.let {
        Text(
            text = text,
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.secondary,
            fontFamily = FontFamily(Font(R.font.poppins_medium)),
            overflow = TextOverflow.Visible
        )

        Spacer(modifier = Modifier.height(8.dp))
    }
}

@Composable
fun BodyInformationText(text: String?) {
    text?.let {
        Text(
            text = text,
            fontSize = 12.sp,
            fontFamily = FontFamily(Font(R.font.poppins_light)),
            overflow = TextOverflow.Visible
        )

        Spacer(modifier = Modifier.height(8.dp))
    }
}

@Composable
fun BodyErrorText(text: String?) {
    text?.let {
        Text(
            text = text,
            color = Color.Red,
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.poppins_semibold)),
            overflow = TextOverflow.Visible
        )
    }
}

@Composable
fun TableRow(drawable:Int, title: String , value: String) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Start
    ) {
        val tint = LocalContentColor.current
        Image(
            painter = painterResource(drawable),
            colorFilter = ColorFilter.tint(tint),
            modifier = Modifier
                .size(24.dp)
                .padding(end = 8.dp),
            contentDescription = title
        )

        Text(
            text = title,
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.poppins_medium)),
        )

        Spacer(Modifier.weight(1f))

        Text(
            modifier = Modifier.padding(start = 16.dp, end = 16.dp),
            text = value,
            fontSize = 16.sp,
            fontFamily = FontFamily(Font(R.font.poppins_medium)),
        )
    }
}