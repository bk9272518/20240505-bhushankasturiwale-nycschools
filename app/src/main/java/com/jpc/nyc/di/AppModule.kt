package com.jpc.nyc.di

import android.content.Context
import com.jpc.nyc.domain.network.SchoolService
import com.jpc.nyc.domain.repository.SchoolNetworkRepository
import com.jpc.nyc.domain.repository.SchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    fun provideContext(@ApplicationContext context: Context): Context {
        return context
    }

    @Singleton
    @Provides
    fun provideDispatchers(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    fun provideSchoolService(): SchoolService {
        return SchoolService()
    }

    @Provides
    fun provideSchoolRepository(schoolService: SchoolService): SchoolRepository {
        return SchoolNetworkRepository(schoolService)
    }
}