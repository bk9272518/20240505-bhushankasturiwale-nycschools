package com.jpc.nyc.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpc.nyc.domain.models.SchoolScore
import com.jpc.nyc.domain.repository.SchoolRepository
import com.jpc.nyc.util.ServiceState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject


data class ScoreUiState(
    val firstLoad: Boolean = true,
    val schoolScoreState: ServiceState<Unit> = ServiceState.NotStarted
)

@HiltViewModel
class ScoreViewModel @Inject constructor(
    private val dispatcher : CoroutineDispatcher = Dispatchers.IO,
    private val schoolRepository: SchoolRepository,
) : ViewModel() {

    private val _state = MutableStateFlow(ScoreUiState())
    val state = _state.asStateFlow()

    private var schoolScores = emptyList<SchoolScore>()

    init {
        getSchoolScores()
    }

    fun getSchoolScore(id: String): SchoolScore? {
        if(state.value.firstLoad) {
            getSchoolScores()
        }
        return schoolScores.firstOrNull { it.id == id }
    }

    private fun getSchoolScores() {
        _state.update {
            it.copy(schoolScoreState = ServiceState.Loading)
        }
        viewModelScope.launch(dispatcher){
            schoolRepository.getSchoolScores().fold(
                onSuccess = { listOfScores ->
                    schoolScores = listOfScores
                    _state.update {
                        it.copy(
                            schoolScoreState = ServiceState.Success(Unit),
                            firstLoad = false
                        )
                    }
                },
                onFailure = { error ->
                    _state.update {
                        it.copy(
                            schoolScoreState = ServiceState.Error(error.message),
                            firstLoad = false
                        )
                    }
                }
            )
        }
    }
}