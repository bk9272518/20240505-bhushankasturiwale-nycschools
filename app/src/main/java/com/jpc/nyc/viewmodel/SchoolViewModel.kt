package com.jpc.nyc.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.repository.SchoolNetworkRepository
import com.jpc.nyc.domain.repository.SchoolRepository
import com.jpc.nyc.util.ServiceState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject


data class SchoolUiState(
    val schoolState: ServiceState<List<School>> = ServiceState.NotStarted
)

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val dispatcher : CoroutineDispatcher = Dispatchers.IO,
    private val schoolNetworkRepository: SchoolRepository,
) : ViewModel() {

    private val _state = MutableStateFlow(SchoolUiState())
    val state = _state.asStateFlow()

    init {
        getSchoolList()
    }

    fun getSchool(id: String): School? {
        return (_state.value.schoolState as? ServiceState.Success)
            ?.data?.firstOrNull { it.id == id }
    }

    private fun getSchoolList() {
        viewModelScope.launch(dispatcher) {
            _state.update {
                it.copy(schoolState = ServiceState.Loading)
            }
            schoolNetworkRepository.getSchoolList().fold(
                onSuccess = { schools ->
                    _state.update {
                        it.copy(schoolState = ServiceState.Success(schools))
                    }
                },
                onFailure = { error ->
                    _state.update {
                        it.copy(schoolState = ServiceState.Error(error.message))
                    }
                }
            )
        }
    }
}