package com.jpc.nyc

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchools: Application() {}