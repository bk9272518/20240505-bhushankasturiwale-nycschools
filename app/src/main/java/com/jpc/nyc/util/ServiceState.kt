package com.jpc.nyc.util

sealed class ServiceState<out T> {
    data object NotStarted : ServiceState<Nothing>()
    data object Loading : ServiceState<Nothing>()
    data class Success<T>(val data: T) : ServiceState<T>()
    data class Error(val message: String?) : ServiceState<Nothing>()
}