package com.jpc.nyc.util

import android.content.Context


fun Context.launchPhoneDialer(phoneNumber: String) {
    //TODO: Intent for calling phone dialer can be implemented with proper permissions
}

fun Context.launchEmail(email: String) {
    //TODO: Intent for calling email can be implemented
}

fun Context.launchMap(latitude: String, longitude: String) {
    //TODO: Intent for calling google map can be implemented
}

fun Context.launchWebLink(url: String) {
    //TODO: Intent for calling google map can be implemented
}
