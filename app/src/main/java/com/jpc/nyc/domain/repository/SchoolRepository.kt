package com.jpc.nyc.domain.repository

import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.SchoolScore

/**
 * Repository which provides access to data through API, File-System, Database etc.
 */
interface SchoolRepository {
    suspend fun getSchoolList(): Result<List<School>>
    suspend fun getSchoolScores(): Result<List<SchoolScore>>
}