package com.jpc.nyc.domain.models.fake

import com.jpc.nyc.domain.models.SchoolScore

/**
 * This is a method which provides fake object for model class for Preview and Unit Test Cases
 */
fun SchoolScore.Companion.fake(): SchoolScore {
    return SchoolScore(
        "17K548",
        "Brooklyn School for Music & Theatre",
        "155",
        "417",
        "447",
        "406"
    )
}