package com.jpc.nyc.domain.repository

import com.google.gson.Gson
import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.SchoolScore
import com.jpc.nyc.domain.network.SchoolService
import javax.inject.Inject

class SchoolNetworkRepository @Inject constructor(
    private val schoolService: SchoolService
) : SchoolRepository {
    override suspend fun getSchoolList(): Result<List<School>> {
        return schoolService.getSchools().fold(
            onSuccess = { response ->
                val result = Gson().fromJson(response, Array<School>::class.java)
                Result.success(result.toList())
            },
            onFailure = {
                Result.failure(it)
            })
    }

    override suspend fun getSchoolScores(): Result<List<SchoolScore>> {
        return schoolService.getSchoolScores().fold(
            onSuccess = { response ->
                val result = Gson().fromJson(response, Array<SchoolScore>::class.java)
                Result.success(result.toList())
            },
            onFailure = {
                Result.failure(it)
            })
    }
}