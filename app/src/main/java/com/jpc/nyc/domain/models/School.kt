package com.jpc.nyc.domain.models

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class School (
    @SerializedName("dbn")
    val id: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("overview_paragraph")
    val description: String? = null,

    @SerializedName("academicopportunities1")
    val academicOpportunity1: String? = null,
    @SerializedName("academicopportunities2")
    val academicOpportunity2: String? = null,

    @SerializedName("extracurricular_activities")
    val extraCurricular: String? = null,
    @SerializedName("school_sports")
    val sports: String? = null,

    @SerializedName("finalgrades")
    val finalGrades: String? = null,
    @SerializedName("total_students")
    val totalStudents: String? = null,

    @SerializedName("phone_number")
    val phoneNumber: String? = null,
    @SerializedName("school_email")
    val email: String? = null,
    @SerializedName("website")
    val website: String? = null,
    @SerializedName("primary_address_line_1")
    val primaryAddress: String? = null,
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("zip")
    val zip: String? = null,
    @SerializedName("state_code")
    val stateCode: String? = null,
    @SerializedName("latitude")
    val latitude: String? = null,
    @SerializedName("longitude")
    val longitude: String? = null,
) {
    val fullAddress: String
        get() = "$primaryAddress, $city, $stateCode $zip"

    companion object
}