package com.jpc.nyc.domain.models.fake

import com.jpc.nyc.domain.models.School

/**
 * This is a method which provides fake object for model class for Preview and Unit Test Cases
 */
fun School.Companion.fake(): School {
    return School(
        "17K548",
        "Brooklyn School for Music & Theatre",
        "Brooklyn School for Music & Theatre (BSMT) uses our academic program to accommodate the intellectual, social, and emotional needs of creative high school students. Our vision is to provide a model professional environment where respect is mutual, ideas are shared, and learning is not limited to the classroom. We prepare students for higher education through our rigorous academic program while simultaneously allowing them to develop professional careers in the music and theatre industries.",
        "CTE program(s) in: Arts, A/V Technology & Communication",
        "iLearnNYC: Program for expanded online coursework and self-paced learning",
        "Chess, The Step Team, Fashion, Tech Team, WomenÂ’s Group; Extensive arts after-school program: Tech, Dance, Drama, and Chorus Companies; Crew program that trains students in running the lights, sound, video, and all backstage and pit crew responsibilities; Saturday and after-school classes for Regents Preparation; School Leadership Team; Student Government; At least three annual major school-wide productions; Two annual talent shows",
        "Cricket, Double Dutch",
        "9-12",
        "352",
        "718-230-6250",
        "www.bkmusicntheatre.com",
        "www.bkmusicntheatre.com",
        "883 Classon Avenue",
        "Brooklyn",
        "11225",
        "NY",
        "40.66981",
        "-73.9607",
        )
}