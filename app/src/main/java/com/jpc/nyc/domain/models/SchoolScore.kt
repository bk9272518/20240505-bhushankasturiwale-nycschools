package com.jpc.nyc.domain.models

import com.google.gson.annotations.SerializedName

data class SchoolScore(
    @SerializedName("dbn")
    val id : String,
    @SerializedName("school_name")
    val schoolName : String,

    @SerializedName("num_of_sat_test_takers")
    val studentsAttendedTest : String?,
    @SerializedName("sat_critical_reading_avg_score" )
    val readingAvgScore : String? = null,
    @SerializedName("sat_math_avg_score")
    var mathAverageScore: String? = null,
    @SerializedName("sat_writing_avg_score")
    var writingAvgScore: String? = null
) {
    companion object
}