package com.jpc.nyc.domain.network

import java.io.BufferedReader
import java.net.HttpURLConnection
import java.net.URL

class SchoolService {

    private val baseUrl = "https://data.cityofnewyork.us/resource/"

    fun getSchools(): Result<String> {
        return makeApiCall("s3k6-pzi2.json")
    }

    fun getSchoolScores(): Result<String> {
        return makeApiCall("f9bf-2cp4.json")
    }

    private fun makeApiCall(api: String): Result<String> {
        return try {
            val url = URL(baseUrl.plus(api))
            val httpURLConnection = url.openConnection() as HttpURLConnection
            val responseCode = httpURLConnection.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {
                val responseText =
                    httpURLConnection.inputStream.bufferedReader().use(BufferedReader::readText)
                httpURLConnection.disconnect()
                Result.success(responseText)
            } else {
                httpURLConnection.disconnect()
                Result.failure(Exception("Error fetching data"))
            }
        } catch (ex: Exception) {
            return Result.failure(ex)
        }
    }
}