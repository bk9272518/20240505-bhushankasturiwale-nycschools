package com.jpc.nyc.ui.screens.list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.jpc.nyc.R
import com.jpc.nyc.component.LinearProgressView
import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.fake.fake
import com.jpc.nyc.util.ServiceState
import com.jpc.nyc.viewmodel.SchoolUiState
import com.jpc.nyc.viewmodel.SchoolViewModel

@Composable
fun SchoolListScreen(
    viewModel: SchoolViewModel,
    navigateToSchoolDetails: (schoolId: String) -> Unit
) {
    val state by viewModel.state.collectAsState()
    SchoolListViewHandle(state, navigateToSchoolDetails)
}

@Composable fun SchoolListViewHandle(
    state: SchoolUiState,
    navigateToSchoolDetails: (schoolId: String) -> Unit) {
    when(state.schoolState) {
        ServiceState.Loading -> {
            LinearProgressView()
        }
        is ServiceState.Success -> {
            SchoolListContents(state.schoolState.data, navigateToSchoolDetails)
        }
        is ServiceState.Error -> {
            NoSchool(
                title = stringResource(id = R.string.error),
                message = stringResource(id = R.string.error_message)
            )
        }
        ServiceState.NotStarted -> {}
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolListContents(
    schoolList: List<School>,
    navigateToSchoolDetails: (schoolId: String) -> Unit
) {
    var searchQuery by rememberSaveable { mutableStateOf("") }

    Scaffold {
        Surface(
            modifier = Modifier.padding(it)
        ) {
            Column(modifier = Modifier.fillMaxSize()) {
                SearchBar(
                    modifier = Modifier
                        .align(Alignment.Start)
                        .fillMaxWidth()
                        .padding(8.dp),
                    query = searchQuery,
                    onQueryChange = { search ->
                        searchQuery = search
                    },
                    onSearch = {},
                    active = false,
                    onActiveChange = {},
                    placeholder = { Text(stringResource(R.string.search_your_school)) },
                    leadingIcon = { Icon(Icons.Default.Search, contentDescription = null) },
                    trailingIcon = {
                        if (searchQuery.isNotEmpty()) {
                            Icon(
                                modifier = Modifier.clickable { searchQuery = "" },
                                imageVector = Icons.Default.Close,
                                contentDescription = stringResource(id = R.string.close)
                            )
                        }
                    }
                ) {
                }

                if (schoolList.isNotEmpty()) {
                    LazyColumn(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(0.dp, 5.dp, 0.dp, 0.dp),
                    ) {
                        items(schoolList.filter { school ->
                            school.schoolName.contains(searchQuery, true)
                        }) { filteredSchool ->
                            SchoolCard(school = filteredSchool, navigateToSchoolDetails)
                        }
                    }
                } else {
                    NoSchool(
                        title = stringResource(R.string.no_school_added),
                        message = stringResource(R.string.no_school_enrolled)
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun SchoolEmptyListScreenPreview() {
    SchoolListContents(emptyList(), navigateToSchoolDetails = {})
}

@Preview
@Composable
fun SchoolListScreenPreview() {
    SchoolListContents(listOf(School.fake()), navigateToSchoolDetails = {})
}