package com.jpc.nyc.ui.screens.details

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.OutlinedCard
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.jpc.nyc.R
import com.jpc.nyc.component.BodyErrorText
import com.jpc.nyc.component.BodyInformationText
import com.jpc.nyc.component.BodyTitleText
import com.jpc.nyc.component.LabelTextWithIcon
import com.jpc.nyc.component.LinearProgressView
import com.jpc.nyc.component.TableRow
import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.SchoolScore
import com.jpc.nyc.util.ServiceState
import com.jpc.nyc.util.launchEmail
import com.jpc.nyc.util.launchMap
import com.jpc.nyc.util.launchPhoneDialer
import com.jpc.nyc.util.launchWebLink
import com.jpc.nyc.viewmodel.ScoreUiState

/**
 * School details view
 */
@Composable
fun SchoolDetailCard(school: School, schoolScore: SchoolScore?, state: ScoreUiState) {
    Column {
        //Description and Academic Details
        SchoolDescription(school)
        Spacer(modifier = Modifier.height(8.dp))

        //Contact Details
        SchoolContactDetails(school)
        Spacer(modifier = Modifier.height(8.dp))

        //Scores
        SchoolScoreHandle(schoolScore, state)
        Spacer(modifier = Modifier.height(8.dp))

        // Opportunities
        SchoolOpportunities(school)
    }
}

@Composable
fun SchoolDescription(school: School) {
    OutlinedCard(
        border = CardDefaults.outlinedCardBorder().copy(0.dp),
        modifier = Modifier
            .padding(10.dp)
            .fillMaxHeight()
            .clickable { },
        shape = RoundedCornerShape(15.dp),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            school.description?.let {
                BodyInformationText(it)
            }

            BodyTitleText(
                "${stringResource(id = R.string.grades)} ${school.finalGrades}  |  ${
                    stringResource(id = R.string.students)
                } ${school.totalStudents}"
            )
        }
    }
}

@Composable
fun SchoolContactDetails(school: School) {
    val context = LocalContext.current

    OutlinedCard(
        border = CardDefaults.outlinedCardBorder().copy(0.dp),
        modifier = Modifier
            .padding(10.dp)
            .fillMaxHeight()
            .clickable { },
        shape = RoundedCornerShape(15.dp),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {

            BodyTitleText(stringResource(R.string.contact_us))

            LabelTextWithIcon(
                text = school.website,
                icon = null,
                contentDescription = stringResource(id = R.string.website),
                isWebLink = true
            ) {
                context.launchWebLink(it)
            }

            LabelTextWithIcon(
                text = school.phoneNumber,
                icon = Icons.Default.Phone,
                contentDescription = stringResource(id = R.string.phone_number)
            ) {
                context.launchPhoneDialer(it)
            }

            LabelTextWithIcon(
                text = school.email,
                icon = Icons.Default.Email,
                contentDescription = stringResource(id = R.string.email)
            ) {
                context.launchEmail(it)
            }

            LabelTextWithIcon(
                text = school.fullAddress,
                icon = Icons.Default.LocationOn,
                contentDescription = stringResource(id = R.string.address)
            ) {
                if (school.latitude != null && school.longitude != null) {
                    context.launchMap(school.latitude, school.longitude)
                }
            }
        }
    }
}

@Composable
fun SchoolScoreHandle(schoolScore: SchoolScore?, state: ScoreUiState) {

    OutlinedCard(
        border = CardDefaults.outlinedCardBorder().copy(0.dp),
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
            .clickable { },
        shape = RoundedCornerShape(15.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 1.dp
        )
    ) {

        when (state.schoolScoreState) {
            ServiceState.Loading -> {
                LinearProgressView()
            }

            is ServiceState.Success -> {
                if (schoolScore != null) {
                    SchoolScore(schoolScore)
                } else {
                    SchoolScoreError(stringResource(id = R.string.school_no_score))
                }
            }

            is ServiceState.Error -> {
                SchoolScoreError(stringResource(id = R.string.error_score_message))
            }

            ServiceState.NotStarted -> {}
        }
    }
}


@Composable
fun SchoolScore(schoolScore: SchoolScore) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        BodyTitleText(stringResource(R.string.scores))

        schoolScore.run {
            studentsAttendedTest?.let {
                TableRow(
                    drawable = R.drawable.students,
                    title = stringResource(R.string.total_test_take),
                    value = it
                )
            }
            Spacer(modifier = Modifier.height(16.dp))

            mathAverageScore?.let {
                TableRow(
                    drawable = R.drawable.math_exam,
                    title = stringResource(R.string.math_avg_scr),
                    value = it
                )
            }
            writingAvgScore?.let {
                TableRow(
                    drawable = R.drawable.write_exam,
                    title = stringResource(R.string.write_avg_scr),
                    value = it
                )
            }
            readingAvgScore?.let {
                TableRow(
                    drawable = R.drawable.read_exam,
                    title = stringResource(R.string.read_avg_scr),
                    value = it
                )
            }
        }
    }
}

@Composable
fun SchoolScoreError(message: String) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BodyErrorText(text = message)
    }
}


@Composable
fun SchoolOpportunities(school: School) {
    OutlinedCard(
        border = CardDefaults.outlinedCardBorder().copy(0.dp),
        modifier = Modifier
            .padding(10.dp)
            .fillMaxHeight()
            .clickable { },
        shape = RoundedCornerShape(15.dp),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {

            BodyTitleText(stringResource(R.string.academic_opportunities))
            BodyInformationText(school.academicOpportunity1)
            BodyInformationText(school.academicOpportunity2)

            school.extraCurricular?.let {
                BodyTitleText(stringResource(R.string.extra_cir_opportunities))
                BodyInformationText(it)
            }

            school.sports?.let {
                BodyTitleText(stringResource(R.string.sports_opportunities))
                BodyInformationText(it)
            }
        }
    }
}




