package com.jpc.nyc.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.jpc.nyc.navigation.SchoolApp
import com.jpc.nyc.ui.theme.NYCSchoolsTheme
import com.jpc.nyc.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen().apply {
            setKeepOnScreenCondition {
                viewModel.state.value
            }
        }

        setContent {
            NYCSchoolsTheme {
                SchoolApp()
            }
        }
    }
}