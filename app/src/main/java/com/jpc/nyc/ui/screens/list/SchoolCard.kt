package com.jpc.nyc.ui.screens.list

import com.jpc.nyc.R
import com.jpc.nyc.domain.models.School
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/**
 * Basic School Card
 */
@Composable
fun SchoolCard(
    school: School,
    navigateToSchoolDetails: (schoolId: String) -> Unit
) {
    OutlinedCard(
        border = CardDefaults.outlinedCardBorder().copy(0.dp),
        modifier = Modifier
            .padding(8.dp)
            .fillMaxHeight()
            .clickable { navigateToSchoolDetails(school.id) },
        shape = RoundedCornerShape(15.dp),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(
                text = school.schoolName,
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.poppins_medium)),
                overflow = TextOverflow.Visible
            )
            Spacer(modifier = Modifier.height(8.dp))

            Row {
                Icon(imageVector = Icons.Default.LocationOn,
                    modifier = Modifier
                        .size(24.dp)
                        .padding(end = 8.dp),
                    contentDescription = stringResource(id = R.string.address))
                Text(
                    text = school.fullAddress,
                    fontSize = 12.sp,
                    fontFamily = FontFamily(Font(R.font.poppins_light)),
                )
            }
        }
    }
}