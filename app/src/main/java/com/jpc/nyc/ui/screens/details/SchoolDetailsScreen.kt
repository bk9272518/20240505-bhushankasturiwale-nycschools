package com.jpc.nyc.ui.screens.details

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.jpc.nyc.R
import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.SchoolScore
import com.jpc.nyc.domain.models.fake.fake
import com.jpc.nyc.util.ServiceState
import com.jpc.nyc.viewmodel.SchoolViewModel
import com.jpc.nyc.viewmodel.ScoreUiState
import com.jpc.nyc.viewmodel.ScoreViewModel

@Composable
fun SchoolDetailsScreen(
    schoolViewModel: SchoolViewModel,
    scoreViewModel: ScoreViewModel,
    schoolId: String,
    navigateBack: () -> Unit
) {
    val state by scoreViewModel.state.collectAsState()

    val schoolResponse = schoolViewModel.getSchool(schoolId)
    val schoolScore = scoreViewModel.getSchoolScore(schoolId)

    schoolResponse?.let {
        SchoolDetailsContent( it, schoolScore, state, navigateBack)
    }
}

@Composable
fun SchoolDetailsContent(
    school: School,
    schoolScore: SchoolScore?,
    state: ScoreUiState,
    navigateBack: () -> Unit
) {

    Scaffold(
        topBar = {
            AppTopBar(school, navigateBack)
        }
    ) {
        Box(
            modifier = Modifier.padding(it)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                SchoolDetailCard(school = school, schoolScore, state)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppTopBar(
    school: School,
    navigateBack: () -> Unit) {
    CenterAlignedTopAppBar(
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.surface
        ),
        title = {
            Text(
                text = school.schoolName,
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.poppins_semibold))
            )
        },
        navigationIcon = {
            IconButton(onClick = {
                navigateBack()
            }) {
                Icon(
                    imageVector = Icons.AutoMirrored.Default.ArrowBack,
                    contentDescription = stringResource(R.string.back)
                )
            }
        })
}

@Preview
@Composable
fun SchoolDetailsScoreScreenPreview() {
    val state = ScoreUiState(
        true,
        ServiceState.Success(Unit)
    )
    SchoolDetailsContent(School.fake() , SchoolScore.fake() , state) {}
}

@Preview
@Composable
fun SchoolDetailsLoadingScreenPreview() {
    val state = ScoreUiState(
        true,
        ServiceState.Loading
    )
    SchoolDetailsContent(School.fake() , SchoolScore.fake() , state) {}
}

@Preview
@Composable
fun SchoolDetailsErrorScreenPreview() {
    val state = ScoreUiState(
        true,
        ServiceState.Error("")
    )
    SchoolDetailsContent(School.fake() , SchoolScore.fake() , state) {}
}