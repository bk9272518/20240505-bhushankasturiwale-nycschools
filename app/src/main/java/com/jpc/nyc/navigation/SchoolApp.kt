package com.jpc.nyc.navigation

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType.Companion.StringType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.jpc.nyc.ui.screens.list.SchoolListScreen
import com.jpc.nyc.ui.screens.details.SchoolDetailsScreen
import com.jpc.nyc.viewmodel.SchoolViewModel
import com.jpc.nyc.viewmodel.ScoreViewModel

@Composable
fun SchoolApp(navController: NavHostController = rememberNavController()) {

    val schoolViewModel = hiltViewModel<SchoolViewModel>()
    val scoreViewModel = hiltViewModel<ScoreViewModel>()

    Scaffold() {
        NavHost(
            navController = navController,
            startDestination = SchoolScreens.SchoolHome.name,
            modifier = Modifier.padding(it),
            enterTransition = {
                slideIntoContainer(
                    towards = AnimatedContentTransitionScope.SlideDirection.Companion.Left,
                    animationSpec = tween(500)
                )
            },
            exitTransition = {
                slideOutOfContainer(
                    towards = AnimatedContentTransitionScope.SlideDirection.Companion.Right,
                    animationSpec = tween(500)
                )
            }
        ) {
            composable(
                route = SchoolScreens.SchoolHome.name,
            ) {
                SchoolListScreen(schoolViewModel) { schoolId ->
                    navController.navigate("${SchoolScreens.SchoolDetails.name}/$schoolId")
                }
            }

            composable(
                route = "${SchoolScreens.SchoolDetails.name}/{schoolId}",
                arguments = listOf(navArgument("schoolId") { type = StringType }),

            ) { backStack ->
                val schoolId = backStack.arguments?.getString("schoolId") ?: "0"
                SchoolDetailsScreen(
                    schoolViewModel,
                    scoreViewModel,
                    schoolId = schoolId,
                    navigateBack = {
                        navController.popBackStack()
                    }
                )
            }
        }
    }
}


