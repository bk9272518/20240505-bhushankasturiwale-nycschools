package com.jpc.nyc.navigation

sealed class SchoolScreens(val name: String) {
    data object SchoolHome : SchoolScreens("school_list")
    data object SchoolDetails : SchoolScreens("school_details")
}
