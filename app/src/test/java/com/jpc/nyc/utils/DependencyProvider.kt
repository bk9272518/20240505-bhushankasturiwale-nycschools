package com.jpc.nyc.utils

import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.SchoolScore
import com.jpc.nyc.domain.repository.SchoolRepository
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import javax.inject.Singleton

fun schoolRepository(
    getSchoolList: suspend () -> Result<List<School>> = {Result.failure(Exception()) },
    getSchoolScores: suspend () -> Result<List<SchoolScore>> = {Result.failure(Exception()) },
) = object: SchoolRepository {
    override suspend fun getSchoolList(): Result<List<School>> {
        return getSchoolList()
    }

    override suspend fun getSchoolScores(): Result<List<SchoolScore>> {
        return getSchoolScores()
    }
}

@Singleton
@Provides
fun provideDispatchers(): CoroutineDispatcher = UnconfinedTestDispatcher()