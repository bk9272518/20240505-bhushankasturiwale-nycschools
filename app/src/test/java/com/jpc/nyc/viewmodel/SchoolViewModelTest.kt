package com.jpc.nyc.viewmodel

import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.fake.fake
import com.jpc.nyc.domain.repository.SchoolRepository
import com.jpc.nyc.util.ServiceState
import com.jpc.nyc.utils.MainDispatcherRule
import com.jpc.nyc.utils.provideDispatchers
import com.jpc.nyc.utils.schoolRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SchoolViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private fun viewModel(
        schoolNetworkRepository: SchoolRepository = schoolRepository()
    ) = SchoolViewModel(
        provideDispatchers(),
        schoolNetworkRepository
    )

    @Test
    fun `load school in progress state`() = runTest {

        //Arrange
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolList = {
                    delay(200)
                    Result.success(listOf(School.fake()))
                }
            )
        )

        //Assert
        assert(viewModel.state.value.schoolState == ServiceState.Loading)
    }

    @Test
    fun `load school in error state`() = runTest {
        //Arrange
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolList = {
                    Result.failure(Throwable("Error"))
                }
            )
        )

        //Assert
        assert(viewModel.state.value.schoolState == ServiceState.Error("Error"))
    }

    @Test
    fun `load school in success state`() = runTest {
        //Arrange
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolList = {
                    Result.success(listOf(School.fake()))
                }
            )
        )

        //Assert
        assert(viewModel.state.value.schoolState == ServiceState.Success(listOf(School.fake())))
    }

    @Test
    fun `get school with school id found`() {
        //Arrange
        val school = School.fake()
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolList = {
                    Result.success(listOf(school))
                }
            )
        )

        val result = viewModel.getSchool(school.id)
        assert(result != null)
    }

    @Test
    fun `get school with school id not found`() {
        //Arrange
        val school = School.fake()
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolList = {
                    Result.success(listOf(school))
                }
            )
        )

        val result = viewModel.getSchool("school.id")
        assert(result == null)
    }

}