package com.jpc.nyc.viewmodel

import com.jpc.nyc.domain.models.School
import com.jpc.nyc.domain.models.SchoolScore
import com.jpc.nyc.domain.models.fake.fake
import com.jpc.nyc.domain.repository.SchoolRepository
import com.jpc.nyc.util.ServiceState
import com.jpc.nyc.utils.MainDispatcherRule
import com.jpc.nyc.utils.provideDispatchers
import com.jpc.nyc.utils.schoolRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ScoreViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private fun viewModel(
        schoolNetworkRepository: SchoolRepository = schoolRepository()
    ) = ScoreViewModel(
        provideDispatchers(),
        schoolNetworkRepository
    )

    @Test
    fun `load score in progress state`() = runTest {

        //Arrange
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolScores = {
                    delay(200)
                    Result.success(listOf(SchoolScore.fake()))
                }
            )
        )

        //Assert
        assert(viewModel.state.value.schoolScoreState == ServiceState.Loading)
    }

    @Test
    fun `load score in error state`() = runTest {
        //Arrange
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolScores = {
                    Result.failure(Throwable("Error"))
                }
            )
        )

        //Assert
        assert(viewModel.state.value.schoolScoreState == ServiceState.Error("Error"))
    }

    @Test
    fun `load score in success state`() = runTest {
        //Arrange
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolScores = {
                    Result.success(listOf(SchoolScore.fake()))
                }
            )
        )

        //Assert
        assert(viewModel.state.value.schoolScoreState == ServiceState.Success(Unit))
    }

    @Test
    fun `get score with school id found`() {
        //Arrange
        val school = School.fake()
        val schoolScore = SchoolScore.fake()
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolScores = {
                    Result.success(listOf(schoolScore))
                }
            )
        )

        val result = viewModel.getSchoolScore(school.id)
        assert(result != null)
    }

    @Test
    fun `get score with school id not found`() {
        //Arrange
        val schoolScore = SchoolScore.fake()
        val viewModel = viewModel(
            schoolNetworkRepository = schoolRepository(
                getSchoolScores = {
                    Result.success(listOf(schoolScore))
                }
            )
        )

        val result = viewModel.getSchoolScore("school.id")
        assert(result == null)
    }

}